# HelloWorld

#### 说明

关于项目需求定义, 请参考 https://gitee.com/how-to-do-it/hello-world/issues

#### 运行应用

开发模式运行

```
mvn clean compile act:run
```

运行端到端测试

```
mvn clean compile act:e2e
```

产品模式运行

```
mvn clean compile package
cd target/dist
tar xzf *.tar.gz
./run
```

#### 已知问题

当使用开发模式启动应用时不能正确现实应用的版本. 这是正常现象, 因为开发模式不会运行 maven build, 无法拿到应用版本信息. 使用产品模式运行应用则没有这个问题, 因为会运行整个 maven build 流程, 其中的 buildnumber 插件会拿到版本号.
