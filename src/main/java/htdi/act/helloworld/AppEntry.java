package htdi.act.helloworld;

import act.Act;
import act.inject.DefaultValue;
import act.util.Output;
import org.osgl.mvc.annotation.GetAction;

public class AppEntry {

    /**
     * The home (`/`) endpoint.
     *
     * @param who
     *      request query parameter to specify the hello target.
     *      default value is `World`.
     */
    @GetAction
    public void home(@DefaultValue("World") @Output String who) {}

    public static void main(String[] args) throws Exception {
        Act.start();
    }

}
