# HelloWorld

#### Description

Refer to https://gitee.com/how-to-do-it/hello-world/issues for project requirement definition

#### Run

Run the app in dev mode

```
mvn clean compile act:run
```

Run end-to-end test

```
mvn clean compile act:e2e
```

Run the app in prod mode

```
mvn clean compile package
cd target/dist
tar xzf *.tar.gz
./run
```

#### Known Issue

When application in DEV mode it won't be able to display the correct application version, instead it shows something like `r${project.version}-${buildNumber}`, this is expected as it won't be able to process the resource file without running the full build process.